/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList();
        for(int i = 0; i < 10; i++){
            list.add(i);
        }
        
        Thread thread1 = new Thread(new Saieyks(list));
        Thread thread2 = new Thread(new Saiekaks(list));
        thread1.start();
        thread2.start();
        
        /*Iterator iterator = list.iterator();
        
        Thread thread3 = new Thread(new Saieyks(list, iterator));
        Thread thread4 = new Thread(new Saiekaks(list, iterator));
        thread3.start();
        thread4.start();*/
        
    }
    
}
