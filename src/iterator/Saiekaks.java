/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Sami
 */
public class Saiekaks implements Runnable {
    
    ArrayList list;
    Iterator iterator;
    boolean isRunning = true;
    int added = 10;
    
    public Saiekaks(ArrayList list){
        this.list = list;
        iterator = list.iterator();
    }
    
    public Saiekaks(ArrayList list, Iterator iterator){
        this.list = list;
        this.iterator = iterator;
    }

    public void run() {
        while(isRunning){
            if(iterator.hasNext()){
                System.out.println("Säije kaksi: "+iterator.next());
                if(list.size() < 20){
                    list.add(added);
                    added++;
                }
                Thread.yield();
            }else{
                isRunning = false;
            }
            
        }
    }
    
}
