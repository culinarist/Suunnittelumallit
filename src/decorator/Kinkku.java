
package decorator;


public class Kinkku extends PizzaDecorator {
    
    private float hinta = 2;
    private String kuvaus = "Kinkku";

    public Kinkku(PizzaIF koristeltavaPohja) {
        super(koristeltavaPohja);
    }
    
    @Override
    public float getHinta(){
        return super.getHinta() + hinta;
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + ", " + kuvaus;
    }
    
}
