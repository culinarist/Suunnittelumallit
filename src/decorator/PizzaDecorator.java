package decorator;

public abstract class PizzaDecorator implements PizzaIF {

    protected PizzaIF koristeltavaOsa;

    public PizzaDecorator(PizzaIF koristeltavaPohja) {
        this.koristeltavaOsa = koristeltavaPohja;
    }

    public float getHinta() {
        return koristeltavaOsa.getHinta();
    }

    public String getKuvaus() {
        return koristeltavaOsa.getKuvaus();
    }

}
