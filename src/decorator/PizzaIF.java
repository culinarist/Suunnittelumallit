
package decorator;


public interface PizzaIF {
    public float getHinta();
    public String getKuvaus();
}
