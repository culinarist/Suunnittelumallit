package decorator;

public class Main {

    public static void main(String[] args) {

        PizzaIF kinkkuAnanas
                = new Ananas(
                        new Kinkku(new Pohja()));
        
        PizzaIF jauhenliha
                = new Jauhenliha(new Pohja());
        
        PizzaIF paprikaJauhenlihaKinkkuAnanas
                = new Ananas(
                    new Kinkku(
                        new Jauhenliha(
                            new Paprika(new Pohja()))));
        
        System.out.println("Pizzan ainekset: "+kinkkuAnanas.getKuvaus()+"\nHinta: "+kinkkuAnanas.getHinta());
        System.out.println("Pizzan ainekset: "+jauhenliha.getKuvaus()+"\nHinta: "+jauhenliha.getHinta());
        System.out.println("Pizzan ainekset: "+paprikaJauhenlihaKinkkuAnanas.getKuvaus()+"\nHinta: "+paprikaJauhenlihaKinkkuAnanas.getHinta());

    }

}
