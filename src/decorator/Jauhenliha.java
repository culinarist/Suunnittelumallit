
package decorator;


public class Jauhenliha extends PizzaDecorator {
    
    private float hinta = 3;
    private String kuvaus = "Jauhenliha";

    public Jauhenliha(PizzaIF koristeltavaPohja) {
        super(koristeltavaPohja);
    }
    
    @Override
    public float getHinta(){
        return super.getHinta() + hinta;
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + ", " + kuvaus;
    }
    
}
