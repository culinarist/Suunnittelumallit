
package decorator;


public class Pohja implements PizzaIF {
    
    private float hinta = 3;
    private String kuvaus = "Pohja";
    
    @Override
    public float getHinta(){
        return hinta;
    }
    
    @Override
    public String getKuvaus(){
        return kuvaus;
    }
    
}
