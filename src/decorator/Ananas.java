/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;


public class Ananas extends PizzaDecorator {
    
    private float hinta = 1;
    private String kuvaus = "Ananas";

    public Ananas(PizzaIF koristeltavaPohja) {
        super(koristeltavaPohja);
    }
    
    @Override
    public float getHinta(){
        return super.getHinta() + hinta;
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + ", " + kuvaus;
    }
    
}
