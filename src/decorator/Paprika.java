
package decorator;


public class Paprika extends PizzaDecorator {
    
    private float hinta = 1;
    private String kuvaus = "Paprika";

    public Paprika(PizzaIF koristeltavaPohja) {
        super(koristeltavaPohja);
    }
    
    @Override
    public float getHinta(){
        return super.getHinta() + hinta;
    }
    
    @Override
    public String getKuvaus(){
        return super.getKuvaus() + ", " + kuvaus;
    }
    
}
