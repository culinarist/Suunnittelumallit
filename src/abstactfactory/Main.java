/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstactfactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Class c = null;
        VaatetehdasIF tehdas = null;

        Properties properties = new Properties();
        try {
            properties.load(
                    new FileInputStream("tehdas.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //luetaan toteuttava tehdas properties-tiedostosta
            c = Class.forName(properties.getProperty("Bosstehdas"));
            tehdas = (VaatetehdasIF) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        JasperJavaKoodaaja jasper = new JasperJavaKoodaaja(tehdas);

        jasper.kerroVaatteet();
    }

}
