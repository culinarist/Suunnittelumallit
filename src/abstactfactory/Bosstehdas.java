
package abstactfactory;

public class Bosstehdas implements VaatetehdasIF {

    public HousutIF luoHousut() {
        return new Bosshousut();
    }

    public LippisIF luoLippis() {
        return new Bosslippis();
    }

    public PaitaIF luoPaita() {
        return new Bosspaita();
    }

    public KenkaIF luoKengat() {
        return new Bosskenka();
    }
    
}
