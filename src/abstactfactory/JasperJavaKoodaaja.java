
package abstactfactory;

public class JasperJavaKoodaaja {
    
    private PaitaIF paita = null;
    private HousutIF housut = null;
    private LippisIF lippis = null;
    private KenkaIF kengat = null;
    
    public JasperJavaKoodaaja(VaatetehdasIF tehdas){
        paita = tehdas.luoPaita();
        housut = tehdas.luoHousut();
        lippis = tehdas.luoLippis();
        kengat = tehdas.luoKengat();
    }
    
    public void kerroVaatteet(){
        System.out.println("Minulla on päällä "+paita+", "+housut+", "+lippis+", "+kengat);
    }
    
}
