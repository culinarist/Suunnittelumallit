
package abstactfactory;

public class Adidastehdas implements VaatetehdasIF {

    public HousutIF luoHousut() {
        return new Adidashousut();
    }

    public LippisIF luoLippis() {
        return new Adidaslippis();
    }

    public PaitaIF luoPaita() {
        return new Adidaspaita();     
    }

    public KenkaIF luoKengat() {
        return new Adidaskenka();
    }
    
}
