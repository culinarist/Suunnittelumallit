/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class Burger {

    private String top = "";
    private String bottom = "";
    private String sauce = "";
    private String steak = "";
    
    private List hesBurgerList = new ArrayList();
    private StringBuilder mcBuilderList = new StringBuilder();

    public void setTop(String top) {
        this.top = top;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }
    
    public void setSteak(String steak) {
        this.steak = steak;
    }
    
    public List getHesBurger(){
        hesBurgerList.add(top);
        hesBurgerList.add(bottom);
        hesBurgerList.add(sauce);
        hesBurgerList.add(steak);
        return hesBurgerList;
    }
    
    public StringBuilder getMcBurger(){
        mcBuilderList.append(top);
        mcBuilderList.append(bottom);
        mcBuilderList.append(sauce);
        mcBuilderList.append(steak);
        return mcBuilderList;
    }

}
