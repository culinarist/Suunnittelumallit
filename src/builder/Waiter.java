/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Sami
 */
public class Waiter {

    private HesburgerBuilder hesBurgerBuilder;
    private McdonaldsBuilder mcBurgerBuilder;
    
    public void setBurgerBuilder(HesburgerBuilder burgerBuilder) {
        hesBurgerBuilder = burgerBuilder;
    }
    public void setBurgerBuilder(McdonaldsBuilder burgerBuilder) {
        mcBurgerBuilder = burgerBuilder;
    }

    public void constructHesBurger() {
        hesBurgerBuilder.buildBottom();
        hesBurgerBuilder.buildSauce();
        hesBurgerBuilder.buildSteak();
        hesBurgerBuilder.buildTop();
    }
    
    public void constructMcBurger() {
        mcBurgerBuilder.buildBottom();
        mcBurgerBuilder.buildSauce();
        mcBurgerBuilder.buildSteak();
        mcBurgerBuilder.buildTop();
    }
}
