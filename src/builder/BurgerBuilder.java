/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.List;

/**
 *
 * @author Sami
 */
public interface BurgerBuilder {

    public abstract void buildTop();
    public abstract void buildBottom();
    public abstract void buildSteak();
    public abstract void buildSauce();
    

}
