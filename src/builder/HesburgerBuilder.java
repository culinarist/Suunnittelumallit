/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class HesburgerBuilder implements BurgerBuilder {
    
    private String top = "";
    private String bottom = "";
    private String sauce = "";
    private String steak = "";
    
    private List hesBurgerList = new ArrayList();

    @Override
    public void buildTop() {
        top = "Sweet Top";
    }

    @Override
    public void buildBottom() {
        bottom = "Sweet bottom";
    }

    @Override
    public void buildSteak() {
        steak = "Sweet steak";
    }

    @Override
    public void buildSauce() {
        sauce = "Sweet sauce";
    }
    
    public List getBurger(){
        hesBurgerList.add(top);
        hesBurgerList.add(bottom);
        hesBurgerList.add(sauce);
        hesBurgerList.add(steak);
        return hesBurgerList;
    }
    
}
