/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class McdonaldsBuilder implements BurgerBuilder {
    
    private String top = "";
    private String bottom = "";
    private String sauce = "";
    private String steak = "";
    
    private StringBuilder mcBuilderList = new StringBuilder();  
    

    @Override
    public void buildTop() {
        top = "Salty top";
    }

    @Override
    public void buildBottom() {
        bottom = "Salty bottom";
    }

    @Override
    public void buildSteak() {
        steak = "Salty steak";
    }

    @Override
    public void buildSauce() {
        sauce = "Chili sauce";
    }
      
    public StringBuilder getBurger(){
        mcBuilderList.append(top);
        mcBuilderList.append(bottom);
        mcBuilderList.append(sauce);
        mcBuilderList.append(steak);
        return mcBuilderList;
    }

}
