/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Waiter waiter = new Waiter();
        McdonaldsBuilder mcDBuilder = new McdonaldsBuilder();
        HesburgerBuilder hesBuilder = new HesburgerBuilder();
        
        waiter.setBurgerBuilder(mcDBuilder);
        waiter.constructMcBurger();
        StringBuilder mcString = mcDBuilder.getBurger();
        
        System.out.println(mcString);
        
        waiter.setBurgerBuilder(hesBuilder);
        waiter.constructHesBurger();
        List hesList = hesBuilder.getBurger();
        
        for(int i = 0; i < hesList.size(); i++){
            System.out.print(hesList.get(i)+" ");
        }
        
    }

}
