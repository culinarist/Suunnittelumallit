/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pokemon p = new Pokemon();
        
        System.out.println("Charmander\n------------------------");
        p.attack();
        p.run();
        p.block();
        
        System.out.println("____________________________\n\n\nCharmeleon\n------------------------");
        p.attack();
        p.run();
        p.block();
        
        System.out.println("____________________________\n\n\nCharizard\n------------------------");
        p.attack();
        p.run();
        p.block();
    }
    
}
