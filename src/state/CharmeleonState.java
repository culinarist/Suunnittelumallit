/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Sami
 */
public class CharmeleonState extends State {
    
    private static CharmeleonState INSTANCE = null;
    
    public static CharmeleonState getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CharmeleonState();
        }
        return INSTANCE;
    }
    
    public void attack(Pokemon p) {
        System.out.println("Fireball!");
        //changeState(p, CharizardState.getInstance());
    }

    public void run(Pokemon p) {
        System.out.println("Stomp stomp stomp.. running...");
        //changeState(p, CharizardState.getInstance());
    }

    public void block(Pokemon p) {
        System.out.println("Block! cling!");
        changeState(p, CharizardState.getInstance());
    }
    
}
