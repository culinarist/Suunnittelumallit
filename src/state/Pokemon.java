/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Sami
 */
public class Pokemon {

    private State state;
//Aluksi ollaan TCPClosed-tilassa

    public Pokemon() {
        state = CharmanderState.getInstance();
    }

    public void attack() {
        state.attack(this);
    }

    public void run() {
        state.run(this);
    }

    public void block() {
        state.block(this);
    }

    protected void changeState(State s) {
        state = s;
    }

}
