/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Sami
 */
abstract class State {
    void attack(Pokemon p){};
    void run(Pokemon p){};
    void block(Pokemon p){};
    void changeState(Pokemon p, State s){
        p.changeState(s);
    }
}
