/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Sami
 */
public class CharmanderState extends State {
    
    private static CharmanderState INSTANCE = null;
    
    public static CharmanderState getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CharmanderState();
        }
        return INSTANCE;
    }
    
    public void attack(Pokemon p) {
        System.out.println("Scratch");
        //changeState(p, CharmeleonState.getInstance());
    }

    public void run(Pokemon p) {
        System.out.println("TipTopTipTop running...");
        //changeState(p, CharmeleonState.getInstance());
    }

    public void block(Pokemon p) {
        System.out.println("Block!");
        changeState(p, CharmeleonState.getInstance());
    }
    
}
