/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Target adaptor = new Adaptor();
        
        System.out.println(adaptor.get120Volt().getVolts()+"Volts");
        System.out.println(adaptor.get12Volt().getVolts()+"Volts");
        System.out.println(adaptor.get3Volt().getVolts()+"Volts");
    }

}
