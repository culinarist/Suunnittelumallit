/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Sami
 */
public class MinuuttiViisari extends Viisari implements Cloneable {
    
    private int minutes;

    public void tick() {
        if(minutes == 59){
            minutes = 0;
        }else{
            minutes++;
        }
    }

    public int getTime() {
        return minutes;
    }
    
    public void setTime(int minutes){
        if(minutes >= 0 && minutes <= 59){
            this.minutes = minutes;
        }else{
            System.out.println("Ei onnistu!");
        }
        
    }
    
}
