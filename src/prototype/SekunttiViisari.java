/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Sami
 */
public class SekunttiViisari extends Viisari implements Cloneable {
    
    private int seconds;

    public void tick() {
        if(seconds == 59){
            seconds = 0;
        }else{
            seconds++;
        }
    }

    public int getTime() {
        return seconds;
    }
    
    public void setTime(int seconds){
        if(seconds >= 0 && seconds <= 59){
            this.seconds = seconds;
        }else{
            System.out.println("Ei onnistu!");
        }
    }
    
}
