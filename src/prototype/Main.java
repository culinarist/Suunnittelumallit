/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Kello k = new Kello();
        
        int hour = 12;
        int min = 30;
        int sec = 30;
                
        k.tuntiViisari.setTime(hour);
        k.minViisari.setTime(min);
        k.secViisari.setTime(sec);
        
        System.out.println("Alkuperäinen kello: "+k.tuntiViisari.getTime()+":"+k.minViisari.getTime()+":"+k.secViisari.getTime());
        
        Kello clonek = k.clone();
        
        System.out.println("Klooni kello: "+clonek.tuntiViisari.getTime()+":"+clonek.minViisari.getTime()+":"+clonek.secViisari.getTime());
           
        hour = 13;
        min = 40;
        sec = 10;
        
        clonek.tuntiViisari.setTime(hour);
        clonek.minViisari.setTime(min);
        clonek.secViisari.setTime(sec);
        
        System.out.println("Klooni kello uudessa ajassa: "+clonek.tuntiViisari.getTime()+":"+clonek.minViisari.getTime()+":"+clonek.secViisari.getTime());
        
        System.out.println("Alkuperäinen kello uuden kloonikellon ajanvaihdon jälkeen: "+k.tuntiViisari.getTime()+":"+k.minViisari.getTime()+":"+k.secViisari.getTime());
        
        for(int i = 0; i < 10; i++){
            clonek.tick();
            k.tick();
            System.out.println("Klooni kello: "+clonek.tuntiViisari.getTime()+":"+clonek.minViisari.getTime()+":"+clonek.secViisari.getTime());
            System.out.println("Alkuperäinen kello: "+k.tuntiViisari.getTime()+":"+k.minViisari.getTime()+":"+k.secViisari.getTime());
        }
        
    }
    
}
