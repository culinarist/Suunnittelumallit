/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Sami
 */
public class TuntiViisari extends Viisari implements Cloneable {
    
    private int hours;

    public void tick() {
        if(hours == 23){
            hours = 0;
        }else{
            hours++;
        }
       
    }

    public int getTime() {
        return hours;
    }
    
    public void setTime(int hours){
        if(hours >= 0 && hours <= 23){
            this.hours = hours;
        }else{
            System.out.println("Ei onnistu!");
        }
        
    }
    
}
