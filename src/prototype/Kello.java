/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Sami
 */
public class Kello implements Cloneable {
    
    Viisari tuntiViisari;
    Viisari minViisari;
    Viisari secViisari;

    public Kello() {
        this.tuntiViisari = new TuntiViisari();
        this.minViisari = new MinuuttiViisari();
        this.secViisari = new SekunttiViisari();
    }

    public Kello clone() {
        Kello s = null;
        try {
            s = (Kello) super.clone();
            s.tuntiViisari = (Viisari) tuntiViisari.clone();
            s.minViisari = (Viisari) minViisari.clone();
            s.secViisari = (Viisari) secViisari.clone();
        } catch (CloneNotSupportedException e) {
        }
        return s;
    }
    
    public void tick(){
        secViisari.tick();
        if(secViisari.getTime() == 0){
            minViisari.tick();
            if(minViisari.getTime() == 0){
                tuntiViisari.tick();
            }
        }
    }

}
