/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author Sami
 */
public class RawMaterialStore implements Store {
    
    public Goods getGoods() {
        RawMaterialGoods rawGoods = new RawMaterialGoods();
        return rawGoods;
    }
}
