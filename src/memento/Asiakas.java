/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Sami
 */
public class Asiakas extends Thread {
    
    private Memento memento;
    private final Arvuuttaja arvuuttaja;
    private final String nimi;
    
    public Asiakas(Arvuuttaja arvuuttaja, String nimi){
        this.arvuuttaja = arvuuttaja;
        this.nimi = nimi;
    }
    
    @Override
    public void run(){
        arvuuttaja.liityPeliin(this);
        int arvaus = 1;
        while (true){
            if (arvuuttaja.kasitteleArvaus(arvaus, memento)){
                System.out.println(nimi + " arvasi numeronsa oikein!");
                break;
            }
            arvaus++;
        }
    }
    
    public void setMemento(Memento memento) {
        this.memento = memento;
    }
    
}
