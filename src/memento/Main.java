/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Arvuuttaja arvuuttaja = new Arvuuttaja();
        
        Asiakas sami = new Asiakas(arvuuttaja, "Sami");
        Asiakas toni = new Asiakas(arvuuttaja, "Toni ");
        Asiakas petra = new Asiakas(arvuuttaja, "Petra");
        Asiakas aiti = new Asiakas(arvuuttaja, "Äiti");
        Asiakas isa = new Asiakas(arvuuttaja, "Isä");
        
        sami.start();
        toni.start();
        petra.start();
        aiti.start();
        isa.start();
    }
    
}
