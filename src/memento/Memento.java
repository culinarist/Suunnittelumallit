/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Sami
 */
public class Memento {
    
    private final int numero;
    
    public Memento(int luku) {
        numero = luku;
    }
    
    public boolean vertaaArvausta(int arvaus){
        if(numero == arvaus){
            return true;
        }
        return false;
    }
    
}
