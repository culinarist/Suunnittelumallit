/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Random;

/**
 *
 * @author Sami
 */
public class Arvuuttaja {
    
    private Random random;
    
    public Arvuuttaja(){
        random = new Random();
    }
    
    public void liityPeliin(Asiakas asiakas){
        asiakas.setMemento(new Memento(random.nextInt(10)+1));
    }
    public boolean kasitteleArvaus(int arvaus, Memento memento){
        return memento.vertaaArvausta(arvaus);
    }
}
