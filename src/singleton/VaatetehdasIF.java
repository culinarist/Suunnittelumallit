
package singleton;

public interface VaatetehdasIF {
    public HousutIF luoHousut();
    public LippisIF luoLippis();
    public PaitaIF luoPaita();
    public KenkaIF luoKengat();
}
