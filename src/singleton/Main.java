/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Class c = null;
        VaatetehdasIF tehdas = null;

        Properties properties = new Properties();
        try {
            properties.load(
                    new FileInputStream("tehdas2.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //luetaan toteuttava tehdas properties-tiedostosta
            c = Class.forName(properties.getProperty("Adidastehdas"));
            Method method = c.getDeclaredMethod("getInstance");

            tehdas = (VaatetehdasIF) method.invoke(null);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JasperJavaKoodaaja jasper = new JasperJavaKoodaaja(tehdas);
        jasper.kerroVaatteet();
    }

}
