
package singleton;

public class Adidastehdas implements VaatetehdasIF {
    
    private static Adidastehdas INSTANCE = null;

    private Adidastehdas() {
    }

    public static Adidastehdas getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Adidastehdas();
        }
        return INSTANCE;
    }

    public HousutIF luoHousut() {
        return new Adidashousut();
    }

    public LippisIF luoLippis() {
        return new Adidaslippis();
    }

    public PaitaIF luoPaita() {
        return new Adidaspaita();     
    }

    public KenkaIF luoKengat() {
        return new Adidaskenka();
    }
    
}
