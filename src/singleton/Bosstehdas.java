package singleton;

public class Bosstehdas implements VaatetehdasIF {
    
    private static Bosstehdas INSTANCE = null;

    private Bosstehdas() {
    }

    public static Bosstehdas getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Bosstehdas();
        }
        return INSTANCE;
    }

    public HousutIF luoHousut() {
        return new Bosshousut();
    }

    public LippisIF luoLippis() {
        return new Bosslippis();
    }

    public PaitaIF luoPaita() {
        return new Bosspaita();
    }

    public KenkaIF luoKengat() {
        return new Bosskenka();
    }

}
