package factorymethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus x = new Oppilas();
        AterioivaOtus y = new Opettaja();
        AterioivaOtus z = new Rehtori();
        x.aterioi();
        y.aterioi();
        z.aterioi();
    }
}
