/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Sami
 */
public abstract class RaiseHandler {

    protected RaiseHandler successor;

    public void setSuccessor(RaiseHandler successor) {
        this.successor = successor;
    }

    public void processRequest(RequestedSalary request){
        successor.processRequest(request);
    }

}
