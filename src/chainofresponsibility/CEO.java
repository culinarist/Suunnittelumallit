/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Sami
 */
public class CEO extends RaiseHandler{
    
    private double raise;
    private double salary;

    private final double ALLOWABLE = 100;

    public void processRequest(RequestedSalary request) {
        
        if (request.getAmount() < (ALLOWABLE * request.getSalary())) {
            System.out.println(
                    "CEO will approve your $" + request.getAmount() + " raise");
        } else if (successor != null) {
            super.processRequest(request);
        }

    }
    
}
