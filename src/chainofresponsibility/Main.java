/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Manager manager = new Manager();
        Boss boss = new Boss();
        CEO ceo = new CEO();
        manager.setSuccessor(boss);
        boss.setSuccessor(ceo);

        // Press Ctrl+C to end.
        try {
            while (true) {
                System.out.println("Enter the amount to check who should approve your raise.");
                System.out.print(">");
                double amount = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
                
                System.out.println("Enter your salary.");
                System.out.print(">");
                double salary = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
                manager.processRequest(new RequestedSalary(amount, salary));
            }
        } catch (Exception e) {
            System.exit(1);
        }
    }
}


