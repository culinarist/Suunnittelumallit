/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Sami
 */
public class RequestedSalary {

    private double amount;
    private double salary;

    public RequestedSalary(double amount, double salary) {
        this.amount = amount;
        this.salary = salary;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    public double getSalary() {
        return salary;
    }

    public void setSalary(double amount) {
        this.salary = salary;
    }

}
