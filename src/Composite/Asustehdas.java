
package Composite;


public class Asustehdas implements KomponenttitehdasIF {

    public AsusEmolevy luoEmolevy() {
        return new AsusEmolevy();
    }

    public AsusProsessori luoProsessori() {
        return new AsusProsessori();
    }

    public AsusNaytonohjain luoNaytonohjain() {
        return new AsusNaytonohjain();
    }

    public AsusKotelo luoKotelo() {
        return new AsusKotelo();
    }

    public AsusVerkkokortti luoVerkkokortti() {
        return new AsusVerkkokortti();
    }

    public AsusMuistipiiri luoMuistipiiri() {
        return new AsusMuistipiiri();    
    }
    
}
