
package Composite;

import abstactfactory.*;

public interface KomponenttitehdasIF {
    public AsusEmolevy luoEmolevy();
    public AsusProsessori luoProsessori();
    public AsusNaytonohjain luoNaytonohjain();
    public AsusKotelo luoKotelo();
    public AsusVerkkokortti luoVerkkokortti();
    public AsusMuistipiiri luoMuistipiiri();
}
