
package Composite;

public class Main {

    public static void main(String[] args) {
        
        Asustehdas tehdas = new Asustehdas();
        TietokoneenKokoaja kokooja = new TietokoneenKokoaja(tehdas);
        
        System.out.println("Totaali hinta: "+kokooja.palautaKoneenHinta());
    }
    
}
