/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class AsusKotelo implements LaiteosaIF {
    
    private float hinta = 100;

    List<LaiteosaIF> komponenttiLista = new ArrayList<LaiteosaIF>();

    public void lisaaKomponentti(LaiteosaIF komponentti) {
        komponenttiLista.add(komponentti);
    }

    public float kerroHinta() {
        for (LaiteosaIF komponentti : komponenttiLista){
            hinta += komponentti.kerroHinta();
        }
        return hinta;
    }
}
