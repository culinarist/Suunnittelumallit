
package Composite;

import java.util.ArrayList;
import java.util.List;


public class AsusEmolevy implements LaiteosaIF {
    
    private float hinta = 150;
    
    List<LaiteosaIF> komponenttiLista = new ArrayList<LaiteosaIF>();

    public void lisaaKomponentti(LaiteosaIF komponentti) {
        komponenttiLista.add(komponentti);
    }

    public float kerroHinta() {
        for (LaiteosaIF komponentti : komponenttiLista){
            hinta += komponentti.kerroHinta();
        }
        return hinta;
    }
    
}
