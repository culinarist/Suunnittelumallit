
package Composite;

public class TietokoneenKokoaja {
    
    private LaiteosaIF emo = null;
    private LaiteosaIF cpu = null;
    private LaiteosaIF gpu = null;
    private LaiteosaIF memo = null;
    private LaiteosaIF kotelo = null;
    private LaiteosaIF verkkokortti = null;
    
    public TietokoneenKokoaja(KomponenttitehdasIF tehdas){
        emo = tehdas.luoEmolevy();
        cpu = tehdas.luoProsessori();
        gpu = tehdas.luoNaytonohjain();
        memo = tehdas.luoMuistipiiri();
        kotelo = tehdas.luoKotelo();
        verkkokortti = tehdas.luoVerkkokortti();
    }
    
    public float palautaKoneenHinta(){
        emo.lisaaKomponentti(verkkokortti);
        emo.lisaaKomponentti(cpu);
        emo.lisaaKomponentti(gpu);
        emo.lisaaKomponentti(memo);
        kotelo.lisaaKomponentti(emo);
        return kotelo.kerroHinta();
    }
}
