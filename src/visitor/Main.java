/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        PokemonVisitor visitor = new PokemonVisitor();
        Pokemon p = new Pokemon(visitor);
        
        System.out.println("Charmander\n------------------------");
        for(int i = 0; i < 5; i++){
            p.attack();
        }
        p.run();
        p.block();
        
        System.out.println("____________________________\n\n\nCharmeleon\n------------------------");
        for(int i = 0; i < 10; i++){
            p.attack();
        }
        p.run();
        p.block();
        
        System.out.println("____________________________\n\n\nCharizard\n------------------------");
        p.attack();
        p.run();
        p.block();
    }
    
}
