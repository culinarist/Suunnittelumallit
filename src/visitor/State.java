/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
abstract class State {
    void attack(Pokemon p, PokemonVisitor visitor){};
    void run(Pokemon p, PokemonVisitor visitor){};
    void block(Pokemon p ,PokemonVisitor visitor){};
    void changeState(Pokemon p, State s){
        p.changeState(s);
    }
}
