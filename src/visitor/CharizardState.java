/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public class CharizardState extends State {
    
    private static CharizardState INSTANCE = null;
    
    public static CharizardState getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CharizardState();
        }
        return INSTANCE;
    }
    
    public void attack(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Fireblast!");
        //changeState(p, CharmanderState.getInstance());
    }

    public void run(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Stomp stomp stomp.. flying...");
        //changeState(p, CharmanderState.getInstance());
    }

    public void block(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Block! cling! rawr!");
        //changeState(p, CharmanderState.getInstance());
    }
    
    
}
