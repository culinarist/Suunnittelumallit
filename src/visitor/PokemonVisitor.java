/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public class PokemonVisitor implements VisitorIF {
    
    public PokemonVisitor(){
        
    }

    public void visit(Pokemon pokemon, CharmanderState s) {
        if(pokemon.getAttackCounter() >= 5){
                s.changeState(pokemon, CharmeleonState.getInstance());
                System.out.println("Your pokemon has evolved!");
            }
    }

    public void visit(Pokemon pokemon, CharmeleonState s) {
         if((pokemon.getAttackCounter()*pokemon.getMultiplier()) >= 5){
                s.changeState(pokemon, CharizardState.getInstance());
                System.out.println("Your pokemon has evolved!");
            }
    }

    public void visit(Pokemon pokemon, CharizardState s) {
        
    }
    
}
