/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public class CharmeleonState extends State {
    
    private static CharmeleonState INSTANCE = null;
    
    public static CharmeleonState getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CharmeleonState();
        }
        return INSTANCE;
    }
    
    public void attack(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Fireball!");
        visitor.visit(p, this);
        //changeState(p, CharmeleonState.getInstance());
    }

    public void run(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Stomp stomp stomp.. running...");
        visitor.visit(p, this);
        //changeState(p, CharmeleonState.getInstance());
    }

    public void block(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Block! cling!");
        visitor.visit(p, this);
        //changeState(p, CharmeleonState.getInstance());
    }

    
}
