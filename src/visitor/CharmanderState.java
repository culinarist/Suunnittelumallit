/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public class CharmanderState extends State {
    
    private static CharmanderState INSTANCE = null;
    
    public static CharmanderState getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CharmanderState();
        }
        return INSTANCE;
    }
    
    public void attack(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Scratch");
        visitor.visit(p, this);
        //changeState(p, CharmeleonState.getInstance());
    }

    public void run(Pokemon p, PokemonVisitor visitor) {
        System.out.println("TipTopTipTop running...");
        visitor.visit(p, this);
        //changeState(p, CharmeleonState.getInstance());
    }

    public void block(Pokemon p, PokemonVisitor visitor) {
        System.out.println("Block!");
        visitor.visit(p, this);
        //changeState(p, CharmeleonState.getInstance());
    }
    
}
