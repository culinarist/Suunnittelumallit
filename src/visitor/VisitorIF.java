/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public interface VisitorIF {
    void visit(Pokemon pokemon, CharmanderState s);
    void visit(Pokemon pokemon, CharmeleonState s);
    void visit(Pokemon pokemon, CharizardState s);
}
