/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Sami
 */
public class Pokemon {

    private State state;
    private double attackCounter;
    private PokemonVisitor visitor;
    private final double multiplier = 0.5;

    public Pokemon(PokemonVisitor visitor) {
        state = CharmanderState.getInstance();
        this.visitor = visitor;
        attackCounter = 0;
    }

    public void attack() {
        state.attack(this, visitor);
        attackCounter++;
    }

    public void run() {
        state.run(this, visitor);
    }

    public void block() {
        state.block(this, visitor);
    }

    protected void changeState(State s) {
        state = s;
        attackCounter = 0;
    }
    
    public double getAttackCounter(){
        return attackCounter;
    }
    
    public double getMultiplier(){
        return multiplier;
    }

}
