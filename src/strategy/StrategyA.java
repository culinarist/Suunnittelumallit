/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author Sami
 */
public class StrategyA implements ListConverter{

    @Override
    public String listToString(List list) {
        
        String string = "";
        
        for(int i = 1; i < list.size()+1; i++){
            if(i % 3 == 0){
                string = string + list.get(i-1)+ "\n";
            }else{
                string = string + list.get(i-1);
            }
            
        }
        return string;
    }
    
}
