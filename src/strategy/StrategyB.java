/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author Sami
 */
public class StrategyB implements ListConverter{
    
    private String[] stringTaulu = new String[2];
    
    @Override
    public String listToString(List list) {
        String[] stringTaulu = new String[list.size()];
        String string = "";
        for(int i = 0; i < list.size(); i++){
            stringTaulu[i] = (String)list.get(i);
        }
        
        for(int i = 1; i < stringTaulu.length+1; i++){
            if(i % 2 == 0){
                string = string + stringTaulu[i-1] + "\n";
            }else{
                string = string + stringTaulu[i-1];
            }
            
        }
        return string;
    }
    
}
