/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Sami
 */
public class StrategyC implements ListConverter{
    
    @Override
    public String listToString(List list) {
        String string = "";
        Iterator iterator = list.iterator();

        
        while(iterator.hasNext()){
            string = string + (String)iterator.next() + "\n";
        }
        return string;
    }
    
}
