/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> myList = new ArrayList();
        
        myList.add("A");
        myList.add("B");
        myList.add("C");
        myList.add("D");
        myList.add("E");
        myList.add("F");
        myList.add("G");
        myList.add("H");
        myList.add("I");
        myList.add("J");
        myList.add("K");
        myList.add("L");
        myList.add("M");
        myList.add("N");
        myList.add("O");
        myList.add("P");
        myList.add("Q");
        
        ListConverter converterA = new StrategyA();
        ListConverter converterB = new StrategyB();
        ListConverter converterC = new StrategyC();
        
        System.out.println(converterA.listToString(myList));
        System.out.println(converterB.listToString(myList));
        System.out.println(converterC.listToString(myList));
        
    }
    
}
