/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package template;

/**
 *
 * @author Sami
 */
public class Ravirata extends Game {

    String winner = "";
    int ravirata;
    int eteneminen;
    int[] players;
    int minimum = 1;
    int maximum = 6;

    @Override
    void initializeGame() {
        ravirata = 30;
        players = new int[playersCount];
        for (int i = 0; i < playersCount; i++) {
            players[i] = ravirata;
        }
    }

    @Override
    void makePlay(int player) {
        eteneminen = minimum + (int) (Math.random() * maximum);
        if((players[player]-eteneminen) <= 0){
            players[player] = 0;
            System.out.println("Player " + (player+1) + ", has ended the race!");
        }else{
            players[player] -= eteneminen;
            System.out.println("Player " + (player+1) + ", has moved " + eteneminen + " squares and is on square " + players[player] + "/" +ravirata);
        }
        
        
        if (players[player] <= 0) {
            winner = Integer.toString(player+1);
        }

    }

    @Override
    boolean endOfGame() {

        for (int player : players) {
            if (player <= 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    void printWinner() {
        System.out.println("Winner is: " + winner);
    }

}
