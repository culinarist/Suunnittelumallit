/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author Sami
 */
public class ScreenDown implements Command {

    WhiteScreen screen;

    public ScreenDown(WhiteScreen screen) {
        this.screen = screen;
    }

    @Override // Command
    public void execute() {
        screen.turnDown();
    }

    
}
