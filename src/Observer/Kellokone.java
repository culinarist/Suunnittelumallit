
package Observer;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Kellokone implements Runnable {
    
    ClockTimer timer;
    
    public Kellokone(ClockTimer timer){
        this.timer = timer;
    }

    public void run() {
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Kellokone.class.getName()).log(Level.SEVERE, null, ex);
            }
            timer.tick();
        }
    }
    
}
