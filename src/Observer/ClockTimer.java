
package Observer;

import java.util.Observable;


public class ClockTimer extends Observable{
    
    private int sec = 0, min = 0, hour = 0;
    
    public int getHour(){
        return hour;
    }
    
    public int getMinute(){
        return min;
    }
    
    public int getSecond(){
        return sec;
    }
    
    void tick(){
        if(sec < 59){
            sec++;
        }else if(min < 59){
            min++;
            sec = 0;
                
        }else if(hour < 23){
            hour++;
            min = 0;
            sec= 0;
        }else{
            hour = 0;
            min = 0;
            sec = 0;
        }
        setChanged();
        notifyObservers(
            new int[]{hour, min, sec}
        );
    }
}
