package Observer;

public class Main {

    public static void main(String[] args) {
        ClockTimer timer = new ClockTimer();
        Kellokone kellokone = new Kellokone(timer);
        
        DigitalClock clock = new DigitalClock();
        
        timer.addObserver(clock);
        
        new Thread(kellokone).start();
        
        
        
    }
    
}
