
package Observer;

import java.util.Observable;
import java.util.Observer;


public class DigitalClock implements Observer {

    public void update(Observable o, Object arg) {
        int[] clock = (int[]) arg;
        System.out.println("Time: " + clock[0] + ":" + clock[1] + ":" + clock[2]);
    }
    
}
